package de.academy.ourawesomeblog.nutzer;

public class NutzerAdminDTO {
    private boolean adminstatus = false;

    public boolean isAdminstatus() {
        return adminstatus;
    }

    public void setAdminstatus(boolean adminstatus) {
        this.adminstatus = adminstatus;
    }
}
