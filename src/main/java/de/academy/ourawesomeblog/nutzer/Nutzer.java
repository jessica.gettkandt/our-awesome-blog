package de.academy.ourawesomeblog.nutzer;

import de.academy.ourawesomeblog.beitrag.Beitrag;
import de.academy.ourawesomeblog.kommentar.Kommentar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Nutzer {

    @Id
    @GeneratedValue
    private int id;


    private String nutzername;
    private String passwort;
    private boolean adminstatus = false;

    @OneToMany(mappedBy = "nutzer")
    private List<Beitrag> beitraege;

    @OneToMany(mappedBy = "nutzer")
    private List<Kommentar> kommentare;


    public Nutzer() {
    }

    public Nutzer(String nutzername, String passwort) {
        this.nutzername = nutzername;
        this.passwort = passwort;
    }

    public int getId() {
        return id;
    }

    public String getNutzername() {
        return nutzername;
    }

    public boolean isAdminstatus() {
        return adminstatus;
    }

    public void setAdminstatus(boolean adminstatus) {
        this.adminstatus = adminstatus;
    }
}
