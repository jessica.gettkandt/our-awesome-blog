package de.academy.ourawesomeblog.nutzer;

public class RegistrierungsDTO {

    private String nutzername;
    private String passwort;


    public RegistrierungsDTO(String nutzername, String passwort) {
        this.nutzername = nutzername;
        this.passwort = passwort;
    }

    public String getNutzername() {
        return nutzername;
    }

    public String getPasswort() {
        return passwort;
    }
}
