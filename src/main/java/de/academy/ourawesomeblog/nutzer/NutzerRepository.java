package de.academy.ourawesomeblog.nutzer;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface NutzerRepository extends CrudRepository<Nutzer, Integer> {

    Nutzer findById(int id);
    Optional<Nutzer> findByNutzernameAndPasswort(String nutzername, String passwort);

    boolean existsByNutzername(String nutzername);

}
