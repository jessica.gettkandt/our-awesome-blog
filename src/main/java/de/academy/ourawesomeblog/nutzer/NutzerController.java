package de.academy.ourawesomeblog.nutzer;

import de.academy.ourawesomeblog.beitrag.Beitrag;
import de.academy.ourawesomeblog.beitrag.BeitragRepository;
import de.academy.ourawesomeblog.kommentar.KommentarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class NutzerController {
    private NutzerRepository nutzerRepository;
    private KommentarRepository kommentarRepository;
    private BeitragRepository beitragRepository;

    @Autowired
    public NutzerController(NutzerRepository nutzerRepository, KommentarRepository kommentarRepository, BeitragRepository beitragRepository) {
        this.nutzerRepository = nutzerRepository;
        this.kommentarRepository = kommentarRepository;
        this.beitragRepository = beitragRepository;
    }

    @GetMapping("/registrieren")
    public String registrierMapping(Model model) {
        model.addAttribute("registrierung", new RegistrierungsDTO("", ""));
        return "registrierungsTemplate";
    }

    @PostMapping("/registrieren")
    public String registrierMapping(@ModelAttribute("registrierung") RegistrierungsDTO registrierungsDTO, BindingResult bindingResult) {

        if (nutzerRepository.existsByNutzername(registrierungsDTO.getNutzername())) {
            bindingResult.addError(new FieldError("registrierung", "nutzername", "Schon vergeben"));
        }
        Nutzer nutzer = new Nutzer(registrierungsDTO.getNutzername(), registrierungsDTO.getPasswort());
        nutzerRepository.save(nutzer);
        return "redirect:/beitraege";
    }


    @GetMapping("/nutzer/{id}")
    public String nutzerMapping(Model model, @PathVariable int id, @ModelAttribute("sessionNutzer") Nutzer sessionNutzer) {

        Nutzer nutzer = nutzerRepository.findById(id);
        NutzerAdminDTO nutzerAdminDTO = new NutzerAdminDTO();
        nutzerAdminDTO.setAdminstatus(nutzer.isAdminstatus());
        List<Beitrag> beitraege = beitragRepository.findByNutzer(nutzer);
        model.addAttribute("nutzer", nutzer);
        model.addAttribute("beitraege", beitraege);
        model.addAttribute("nutzerAdminDTO", nutzerAdminDTO);
        return "nutzerTemplate";
    }

    @PostMapping("/nutzer/{id}")
    public String adminStatus(@ModelAttribute("sessionNutzer") Nutzer sessionNutzer, @ModelAttribute("nutzerAdminDTO") NutzerAdminDTO nutzerAdminDTO, @PathVariable int id) {
        Nutzer nutzer = nutzerRepository.findById(id);
        nutzer.setAdminstatus(nutzerAdminDTO.isAdminstatus());
        nutzerRepository.save(nutzer);
        return "redirect:/nutzer/" + id;
    }
}
