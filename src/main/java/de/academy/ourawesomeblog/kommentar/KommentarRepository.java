package de.academy.ourawesomeblog.kommentar;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface KommentarRepository extends CrudRepository<Kommentar, Integer> {

    List<Kommentar> findByBeitragIdOrderByKommentarDatumAsc(int id);

    List<Kommentar> findAll();

    Kommentar findById(int kid);

}
