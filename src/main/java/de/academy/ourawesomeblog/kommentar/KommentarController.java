package de.academy.ourawesomeblog.kommentar;

import de.academy.ourawesomeblog.beitrag.Beitrag;
import de.academy.ourawesomeblog.beitrag.BeitragDTO;
import de.academy.ourawesomeblog.beitrag.BeitragRepository;
import de.academy.ourawesomeblog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class KommentarController {

    KommentarRepository kommentarRepository;
    BeitragRepository beitragRepository;


    @Autowired
    public KommentarController(KommentarRepository kommentarRepository, BeitragRepository beitragRepository) {
        this.kommentarRepository = kommentarRepository;
        this.beitragRepository = beitragRepository;
    }

    @GetMapping("/beitrag/{id}")
    public String kommentarMapping(Model model, @PathVariable int id, @ModelAttribute("sessionNutzer") Nutzer sessionNutzer) {

        KommentarDTO kommentar = new KommentarDTO();
        Beitrag beitrag = beitragRepository.findById(id);
        List<Kommentar> kommentare = kommentarRepository.findByBeitragIdOrderByKommentarDatumAsc(id);
        model.addAttribute("beitrag", beitrag);
        model.addAttribute("kommentar", kommentar);
        model.addAttribute("kommentare", kommentare);

        return "kommentarTemplate";
    }

    @PostMapping("/beitrag/{id}")
    public String kommentarPostMapping(@ModelAttribute("kommentar") KommentarDTO kommentarDTO, @ModelAttribute("sessionNutzer") Nutzer sessionNutzer,
                                       @PathVariable int id) {

        Kommentar kommentar = new Kommentar(kommentarDTO.getKommentarInhalt(), sessionNutzer);

        kommentar.setBeitrag(beitragRepository.findById(id));

        kommentarRepository.save(kommentar);

        return "redirect:/beitrag/" + id;
    }

    @PostMapping("/beitrag/delete/{kid}")
    public String kommentarEntfernen(@PathVariable int kid,
                                     @ModelAttribute("sessionNutzer") Nutzer sessionNutzer) {

        Kommentar kommentar = kommentarRepository.findById(kid);
        
        kommentarRepository.delete(kommentar);

        return "redirect:/beitrag/" + kommentar.getBeitrag().getId();
    }

    @GetMapping("/beitrag/edit/{id}")
    public String beitragBearbeiten(Model model, @PathVariable int id) {
        Beitrag beitrag = beitragRepository.findById(id);
        model.addAttribute("beitrag", beitrag);

        return "beitragBearbeiten";
    }

    @PostMapping("/beitrag/edit/{id}")
    public String beitragBearbeitenSpeichern(@ModelAttribute("beitrag") BeitragDTO beitragDTO, @PathVariable int id) {
        Beitrag beitrag = beitragRepository.findById(id);
        beitrag.setBeitragTitel(beitragDTO.getBeitragTitel());
        beitrag.setBeitragText(beitragDTO.getBeitragText());
        beitragRepository.save(beitrag);
        return "redirect:/beitrag/" + id;
    }

}
