package de.academy.ourawesomeblog.kommentar;

import de.academy.ourawesomeblog.nutzer.Nutzer;

public class KommentarDTO {

    private String kommentarInhalt;

    private Nutzer nutzer;

    public Nutzer getNutzer() {
        return nutzer;
    }

    public void setNutzer(Nutzer nutzer) {
        this.nutzer = nutzer;
    }

    public String getKommentarInhalt() {
        return kommentarInhalt;
    }

    public void setKommentarInhalt(String kommentarInhalt) {
        this.kommentarInhalt = kommentarInhalt;
    }
}
