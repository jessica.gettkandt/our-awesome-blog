package de.academy.ourawesomeblog.kommentar;

import de.academy.ourawesomeblog.beitrag.Beitrag;
import de.academy.ourawesomeblog.nutzer.Nutzer;

import javax.persistence.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

@Entity
public class Kommentar {
    @Id
    @GeneratedValue
    private int id;
    private Instant kommentarDatum;
    private String kommentarInhalt;
    @Transient
    private String formattiertesDatum;
    @ManyToOne
    private Beitrag beitrag;

    @ManyToOne
    private Nutzer nutzer;

    public Nutzer getNutzer() {
        return nutzer;
    }

    public Kommentar() {
    }

    public Kommentar(String kommentarInhalt) {
        this.kommentarInhalt = kommentarInhalt;
        this.kommentarDatum = Instant.now();
    }

    public Kommentar(String kommentarInhalt, Nutzer nutzer) {
        this.kommentarInhalt = kommentarInhalt;
        this.nutzer = nutzer;
        this.kommentarDatum = Instant.now();
    }

    public String getFormattiertesDatum() {
        DateTimeFormatter formatter =
                DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)
                        .withLocale(Locale.GERMAN)
                        .withZone(ZoneId.systemDefault());
        return formatter.format(this.getKommentarDatum());
    }

    public int getId() {
        return id;
    }


    public Instant getKommentarDatum() {
        return kommentarDatum;
    }

    public String getKommentarInhalt() {
        return kommentarInhalt;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setKommentarInhalt(String kommentarInhalt) {
        this.kommentarInhalt = kommentarInhalt;
    }

    public void setFormattiertesDatum(String formattiertesDatum) {
        this.formattiertesDatum = formattiertesDatum;
    }
}

