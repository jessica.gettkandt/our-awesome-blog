package de.academy.ourawesomeblog.beitrag;

import de.academy.ourawesomeblog.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class BeitragController {

    BeitragRepository beitragRepository;

    @Autowired
    public BeitragController(BeitragRepository beitragRepository) {
        this.beitragRepository = beitragRepository;
    }

    @GetMapping("/beitraege")
    public String beitraegeMapping(Model model, @ModelAttribute("sessionNutzer") Nutzer sessionNutzer) {
        List<Beitrag> beitraege = beitragRepository.findAllByOrderByBeitragDatumDesc();
        Beitrag beitrag = new Beitrag();
        Nutzer nutzer = new Nutzer();

        model.addAttribute("beitraege", beitraege);
        model.addAttribute("beitrag", beitrag);
        model.addAttribute("nutzer", nutzer);

        return "beitragTemplate";
    }

    @PostMapping("/beitraege")
    public String beitragErstellen(@ModelAttribute("beitrag") BeitragDTO beitragDTO, @ModelAttribute("sessionNutzer") Nutzer sessionNutzer) {
        Beitrag beitrag = new Beitrag(beitragDTO.getBeitragTitel(), beitragDTO.getBeitragText(), sessionNutzer);
        beitragRepository.save(beitrag);
        return "redirect:/beitraege";
    }

    @PostMapping("/beitrag/deletefull/{id}")
    public String beitragUndKommentareLöschen(@ModelAttribute("sessionNutzer") Nutzer sessionNutzer, @PathVariable int id) {

        Beitrag beitrag = beitragRepository.findById(id);


        beitragRepository.delete(beitrag);
        return "redirect:/beitraege";
    }

}
