package de.academy.ourawesomeblog.beitrag;

import de.academy.ourawesomeblog.nutzer.Nutzer;

public class BeitragDTO {
    private String beitragTitel;
    private String beitragText;
    private Nutzer nutzer;

    public Nutzer getNutzer() {
        return nutzer;
    }

    public void setNutzer(Nutzer nutzer) {
        this.nutzer = nutzer;
    }

    public String getBeitragTitel() {
        return beitragTitel;
    }

    public void setBeitragTitel(String beitragTitel) {
        this.beitragTitel = beitragTitel;
    }

    public String getBeitragText() {
        return beitragText;
    }

    public void setBeitragText(String beitragText) {
        this.beitragText = beitragText;
    }
}
