package de.academy.ourawesomeblog.beitrag;

import de.academy.ourawesomeblog.nutzer.Nutzer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BeitragRepository extends CrudRepository<Beitrag, Integer> {
    List<Beitrag> findAllByOrderByBeitragDatumDesc();

    List<Beitrag> findAll();

    List<Beitrag> findByNutzer(Nutzer nutzer);

    Beitrag findById(int id);

}