package de.academy.ourawesomeblog.beitrag;


import de.academy.ourawesomeblog.kommentar.Kommentar;
import de.academy.ourawesomeblog.nutzer.Nutzer;

import javax.persistence.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;


@Entity
public class Beitrag {
    @Id
    @GeneratedValue
    private int id;

    private Instant beitragDatum;
    private String beitragTitel;
    @Lob
    private String beitragText;
    @Transient
    private String formattiertesDatum;

    @ManyToOne
    private Nutzer nutzer;

    @OneToMany(mappedBy = "beitrag", cascade = CascadeType.ALL)
    private List<Kommentar> kommentare;

    public Beitrag() {
    }

    public Beitrag(String beitragTitel, String beitragText, Nutzer nutzer) {
        this.beitragDatum = Instant.now();
        this.beitragTitel = beitragTitel;
        this.beitragText = beitragText;
        this.nutzer = nutzer;
    }

    public int getId() {
        return id;
    }

    public Instant getBeitragDatum() {
        return beitragDatum;
    }

    public String getBeitragTitel() {
        return beitragTitel;
    }

    public String getBeitragText() {
        return beitragText;
    }

    public String getFormattiertesDatum() {
        DateTimeFormatter formatter =
                DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)
                        .withLocale(Locale.GERMAN)
                        .withZone(ZoneId.systemDefault());
        return formatter.format(this.getBeitragDatum());
    }

    public Nutzer getNutzer() {
        return nutzer;
    }

    public void setBeitragTitel(String beitragTitel) {
        this.beitragTitel = beitragTitel;
    }

    public void setBeitragText(String beitragText) {
        this.beitragText = beitragText;
    }
}
