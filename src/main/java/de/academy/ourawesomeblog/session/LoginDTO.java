package de.academy.ourawesomeblog.session;

public class LoginDTO {

    private String nutzername;
    private String passwort;

    public LoginDTO(String nutzername, String passwort) {
        this.nutzername = nutzername;
        this.passwort = passwort;
    }

    public String getNutzername() {
        return nutzername;
    }

    public String getPasswort() {
        return passwort;
    }
}
