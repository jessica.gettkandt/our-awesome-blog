package de.academy.ourawesomeblog.session;

import de.academy.ourawesomeblog.nutzer.Nutzer;
import de.academy.ourawesomeblog.nutzer.NutzerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.Optional;

@Controller
public class SessionController {

    private SessionRepository sessionRepository;
    private NutzerRepository nutzerRepository;

    @Autowired
    public SessionController(SessionRepository sessionRepository, NutzerRepository nutzerRepository) {
        this.sessionRepository = sessionRepository;
        this.nutzerRepository = nutzerRepository;
    }

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("login", new LoginDTO("", ""));
        return "beitraglogin";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute("login") LoginDTO login, BindingResult bindingResult, HttpServletResponse response) {
        Optional<Nutzer> optionalNutzer = nutzerRepository.findByNutzernameAndPasswort(login.getNutzername(), login.getPasswort());

        if (optionalNutzer.isPresent()) {
            Session session = new Session(optionalNutzer.get(), Instant.now().plusSeconds(7 * 24 * 60 * 60));
            sessionRepository.save(session);

            Cookie cookie = new Cookie("sessionId", session.getId());
            response.addCookie(cookie);

            return "redirect:/beitraege";
        }

        bindingResult.addError(new FieldError("login", "passwort", "Login fehlgeschlagen."));

        return "beitraglogin";
    }

    @PostMapping("/logout")
    public String logout(@CookieValue(value = "sessionId", defaultValue = "") String sessionId, HttpServletResponse response) {
        Optional<Session> optionalSession = sessionRepository.findByIdAndExpiresAtAfter(sessionId, Instant.now());
        optionalSession.ifPresent(session -> sessionRepository.delete(session));

        Cookie cookie = new Cookie("sessionId", "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);

        return "redirect:/beitraege";
    }

}
