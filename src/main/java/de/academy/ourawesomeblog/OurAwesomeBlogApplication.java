package de.academy.ourawesomeblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OurAwesomeBlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(OurAwesomeBlogApplication.class, args);
    }

}
